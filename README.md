# prom-metrics-generator

From: https://github.com/arpendu11/node-prometheus-grafana

 Start the Docker containers:
```bash
docker-compose up -d
```

## Add metrics

- Prometheus should be accessible via [http://localhost:9090](http://localhost:9090)
- Grafana should be accessible via [http://localhost:3000](http://localhost:3000)
- Example Node.js server metrics for monitoring should be accessible via [http://localhost:8080/metrics](http://localhost:8080/metrics)
- Example Node.js slow URL should be accessible via [http://localhost:8080/slow](http://localhost:8080/slow)

### Open monitoring dashboards

Open in your web browser the monitoring dashboards:

- [NodeJS Application Dashboard](http://localhost:3000/d/PTSqcpJWk/nodejs-application-dashboard)
- [High Level Application metrics](http://localhost:3000/d/OnjTYJg7k/high-level-application-metrics)
- [Node Service Level Metrics Dashboard](http://localhost:3000/d/WBxkVyRnz/node-service-level-metrics-dashboard)
- [NodeJS Request Flow Dashboard](http://localhost:3000/d/2Er5E1R7k/nodejs-request-flow-dashboard)

### Grafana

- Go to Dashboards/Manage
- Chose NodeJS Application Dashboard

### Prometheus

#### Integration:

- Project settings, integrations, Prometheus
  - check "Active"
  - paste the url (eg: https://9090-amaranth-crow-hysq1e5k.ws-eu17.gitpod.io)
  - click on "Test settings"
  - click on "Save changes"

#### Add new metric

Prometheus metric example
- Metric name`node_nodejs_eventloop_lag_seconds`
- PromQL: `node_nodejs_eventloop_lag_seconds{instance="node-application-monitoring-app:8080",job="node-application-monitoring-app"}`
- Y-axis label: `eventloop_lag_seconds`
- Unit label: `seconds`
- Legend label (optional): `eventloop lag`
- Click on "Create Metric"


- Get the url of Prometheus `gp url 9090`
- Get the url of Grafana `gp url 3000`
- Get the url of application metrics `echo $(gp url 8080)/metrics`


## [DRAFT] Various (to investigate)

```
metric_name{instance="%{ci_environment_slug}-pico.192.168.8.100.nip.io:80",job="consul"}
```

#### Environment(s) and url(s)

- use `CI_ENVIRONMENT_SLUG`

#### Metrics

- Add Prometheus server
- Create metric(s): `__metric_name__{instance="%{ci_environment_slug}-pico.192.168.8.100.nip.io:80",job="consul"}`

```
centroid_x{instance="%{ci_environment_slug}-pico.192.168.8.100.nip.io:80",job="consul"}
centroid_y{instance="%{ci_environment_slug}-pico.192.168.8.100.nip.io:80",job="consul"}
```
